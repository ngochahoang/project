﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ExtraClassLibrary.Models;
using ExtraClassLibrary.MD5Hash;

namespace ExtraClassLibrary.DataAccess
{
    public class SqlConnector : IDataConnection
    {
        private const string db = "ExtraClass";
        public UserModel CreateUser(UserModel model)
        {
          using(System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@UserName", model.UserName);
                param.Add("@Email", model.Email);
                param.Add("@Password", MD5.MD5Hash(model.Password));
                param.Add("@Address", model.Address);
                param.Add("@IdRole", model.IdRole);
                param.Add("@id", 0, System.Data.DbType.Int32, direction: ParameterDirection.Output);
               
                connection.Execute("dbo.sp_user_insert", param, commandType: CommandType.StoredProcedure);
                model.Id = param.Get<int>("@id");
                return model;

            }
            
        }

        public List<RoleModel> GetAllRoles()
        {
            List<RoleModel> roles;
            
            using(IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                roles = connection.Query<RoleModel>("dbo.sp_get_all_roles").ToList();
            }
            return roles;
        }

        public RoleModel GetRoleById(RoleModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                model = connection.QueryFirst<RoleModel>("dbo.sp_get_role", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }
        public List<UserModel> GetAllUsers()
        {
            List<UserModel> users;
            using(IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                users = connection.Query<UserModel>("dbo.sp_get_all_person").ToList();
            }
            return users;
        }

        public UserModel GetUserById(UserModel model)
        {

            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                model = connection.QueryFirst<UserModel>("dbo.sp_get_person",param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

        public UserModel UpdateUser(UserModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                param.Add("@UserName", model.UserName);
                param.Add("@Email", model.Email);
                param.Add("@Password", MD5.MD5Hash(model.Password));
                param.Add("@Address", model.Address);
                param.Add("@IdRole", model.IdRole);
                connection.Execute("dbo.sp_update_person", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

        public UserModel DeleteUser(UserModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                connection.Execute("dbo.sp_delete_person", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

        public List<UserModel> GetUserByRole(RoleModel model)
        {
            List<UserModel> teachers = new List<UserModel>();
            using(IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@RoleName",model.RoleName);
                // teachers = connection.Query<PersonModel>.("dbo.sp_get_user_by_role",param).ToList();
                teachers = connection.Query<UserModel>("dbo.sp_get_user_by_role", param, commandType: CommandType.StoredProcedure).ToList();
                return teachers;
            }
        }

        public ClassModel CreateClass(ClassModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@ClassName", model.ClassName);
                param.Add("@IdTeacher", model.IdTeacher);
                param.Add("@Description", model.Description);
                param.Add("@id", 0, System.Data.DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.sp_class_insert", param, commandType: CommandType.StoredProcedure);
                model.Id = param.Get<int>("@id");
                return model;
            }
        }

        public List<ClassModel> GetAllClass()
        {
            List<ClassModel> classes = new List<ClassModel>();
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                classes = connection.Query<ClassModel>("dbo.sp_get_all_class").ToList();
                return classes;  
            }

        }
        public ClassModel GetClassById(ClassModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                model = connection.QueryFirst<ClassModel>("dbo.sp_get_class", param, commandType: CommandType.StoredProcedure);
                return model;
            }

        }

        public ClassModel UpdateClass(ClassModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                param.Add("@ClassName", model.ClassName);
                param.Add("@IdTeacher", model.IdTeacher);
                param.Add("@Description", model.Description);
                connection.Execute("dbo.sp_update_class", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }
        
        public void RegisterUser(UserModel model)
        {
            using (System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@UserName", model.UserName);
                param.Add("@Email", model.Email);
                param.Add("@Password", MD5.MD5Hash(model.Password));
                param.Add("@Address", model.Address);
                param.Add("@id", 0, System.Data.DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.sp_user_register", param, commandType: CommandType.StoredProcedure);
                model.Id = param.Get<int>("@id");
              

            }
        }

        public List<UserModel> Login(UserModel model)
        {
            List<UserModel> users = new List<UserModel>();
            using (System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {  
                var param = new DynamicParameters();
                param.Add("@Email", model.Email);
                param.Add("@Password", model.Password);
                users = connection.Query<UserModel>("dbo.sp_login", param,commandType:CommandType.StoredProcedure).ToList();
                return users;
            }
            
        }

        public ClassStudentModel CreateClassStudent(UserModel user,ClassModel class1)
        {
            ClassStudentModel classStudent = new ClassStudentModel();
            using (System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var param = new DynamicParameters();
                param.Add("@idclass", class1.Id);
                param.Add("@idstudent", user.Id);
          
                param.Add("@id", 0, System.Data.DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.sp_register_class", param, commandType: CommandType.StoredProcedure);
                classStudent.Id = param.Get<int>("@id");
                return classStudent;

            }

        }
    }

}
