﻿using ExtraClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExtraClassLibrary.DataAccess
{
    public interface IDataConnection
    {
        UserModel CreateUser (UserModel model);
        List<RoleModel> GetAllRoles();
        RoleModel GetRoleById(RoleModel model);
        List<UserModel> GetAllUsers();
        UserModel GetUserById(UserModel model);
        UserModel UpdateUser(UserModel model);
        UserModel DeleteUser(UserModel model);
        List<UserModel> GetUserByRole(RoleModel model);
        ClassModel CreateClass(ClassModel model);
        List<ClassModel> GetAllClass();
        ClassModel GetClassById(ClassModel model);
        ClassModel UpdateClass(ClassModel model);
        void RegisterUser(UserModel model);
        List<UserModel> Login(UserModel user);

        ClassStudentModel CreateClassStudent(UserModel user, ClassModel class1);
        
    }
}
