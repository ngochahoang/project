﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraClassLibrary.Models
{
    public class UserModel
    {
        public UserModel(){}
        public UserModel(int id)
        {
            Id = id;
        }

        public UserModel(string password, string email)
        {
            Password = password;
            Email = email;
        }

        public UserModel(string userName, string password, string email, string address)
        {
            UserName = userName;
            Password = password;
            Email = email;
            Address = address;
        }

        public UserModel(string userName, string password, string email, string address,int idRole)
        {
            UserName = userName;
            Password = password;
            Email = email;
            Address = address;
            IdRole = idRole;
        }

        public UserModel(int id,string userName,string password,string email,string address,int idRole)
        {
            Id = id;
            UserName = userName;
            Password = password;
            Email = email;
            Address = address;
            IdRole = idRole;
        }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int IdRole { get; set; }

        public string TeacherInformation
        {
            get
            {
                return $"{UserName} - {Email}";
            }
        }
  

        
    }
}
