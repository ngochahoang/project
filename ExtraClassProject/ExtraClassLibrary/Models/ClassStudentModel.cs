﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraClassLibrary.Models
{
    public class ClassStudentModel
    {
        public int Id;
        public int IdClass;
        public int IdStudent;

        public ClassStudentModel()
        {
        }

        public ClassStudentModel(int id)
        {
            Id = id;
        }

        public ClassStudentModel(int idClass, int idStudent)
        {
            IdClass = idClass;
            IdStudent = idStudent;
        }
    }

}
