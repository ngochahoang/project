﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraClassLibrary.Models
{
    public class ClassModel
    {
        public ClassModel() { }

        public ClassModel(int id)
        {
            Id = id;
        }

        public ClassModel(string className, int idTeacher, string description)
        {
            ClassName = className;
            IdTeacher = idTeacher;
            Description = description;
        }

        public ClassModel(int id, string className, int idTeacher, string description) : this(id)
        {
            ClassName = className;
            IdTeacher = idTeacher;
            Description = description;
        }

        public int Id;
        public string ClassName { get; set; }
        public int IdTeacher { get; set; }
        public string Description { get; set; }

        public string[] ClassInformation(string id,string className,string teacher,string description)
        {
            string[] arrInformation = new string[] { id, className, teacher, description };
            return arrInformation;
        }



    }
}
