﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraClassLibrary.Models
{
    public class ScoreModel
    {
        public int Id;
        public int IdClass { get; set; }
        public int IdStudent { get; set; }

        public double score;
    }
}
