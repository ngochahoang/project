﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtraClassLibrary;
using ExtraClassLibrary.Models;

namespace ExtraClassUI
{
    public partial class LoginForm : Form
    {
        private UserModel user;
        private List<UserModel> users = new List<UserModel>();
        private RoleModel role;
        private const string roleadmin = "Admin";
        private const string roleteacher = "Teacher";
        private const string rolestudent = "Student";
        public LoginForm()
        {
            InitializeComponent();
        }

        private void SigninBtn_Click(object sender, EventArgs e)
        {
            user = new UserModel(txtPassword.Text.Trim(), txtEmail.Text.Trim());
            users = GlobalConfig.Connection.Login(user);
            int idRole = 0;
            string roleName;
            if(users.Count > 0)
            {
               foreach(UserModel user in users)
                   idRole = user.IdRole;
                roleName = CheckRole(idRole);
                RedirectUserByRole(roleName);


            }
            else
            {
                MessageBox.Show("Email or Password incorrect. Please check it again!", "Error Message", MessageBoxButtons.OK);
            }
           
        }

        private string CheckRole(int id)
        {
            string roleName;
          //  role = new RoleModel(id);
            role = GlobalConfig.Connection.GetRoleById(new RoleModel(id));
            if(role.RoleName == roleadmin)
            {
                roleName = "Admin";
            }
            else if(role.RoleName == roleteacher)
            {
                roleName = "Teacher";
            }
            else
            {
                roleName = "Student";
            }
            return roleName;
        }

        private void RedirectUserByRole(string roleName)
        {
            switch (roleName)
            {
                case roleadmin:
                    {
                        Form adminForm = new Admin.DashBoard();
                        adminForm.Show();
                        break;
                    }
                case rolestudent:
                    {
                        Form studentForm = new Student.DashBoard();
                        studentForm.Show();
                        break;
                    }
                case roleteacher:
                    {
                        Form teacherForm = new Teacher.DashBoard();
                        teacherForm.Show();
                        break;
                    }

            }

        }
        private void TxtEmail_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
