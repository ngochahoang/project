﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtraClassLibrary;
using ExtraClassLibrary.Models;

namespace ExtraClassUI.Student
{
    public partial class RegisterClass : Form
    {
        private List<ClassModel> classes = GlobalConfig.Connection.GetAllClass();
        private UserModel user;
        public RegisterClass()
        {
            InitializeComponent();
            LoadDataGridView();
            dgvClasses.Columns[0].Visible = false;
        
            
        }

        private void LoadDataGridView()
        {
            string[] columnsName = new string[]{ "ID","Class", "Teacher", "Description" };
            FillColumnName(dgvClasses, columnsName, 4);
            foreach (ClassModel model in classes)
            {
                user = GetUser(model.IdTeacher);
                string[] row = model.ClassInformation((model.Id).ToString(),model.ClassName, user.UserName, model.Description);
                dgvClasses.Rows.Add(row);
            }
            AddColumnRegister();

        }

        private void AddColumnRegister()
        {
            DataGridViewButtonColumn btnRegisterClass = new DataGridViewButtonColumn();
            dgvClasses.Columns.Add(btnRegisterClass);
            btnRegisterClass.HeaderText = "Register";
            btnRegisterClass.Text = "Register";
            btnRegisterClass.Name = "btnRegister";
            btnRegisterClass.UseColumnTextForButtonValue = true;
        }
        private UserModel GetUser(int id)
        {
            user = GlobalConfig.Connection.GetUserById(new UserModel(id));
            return user;
        }

        private void FillColumnName(DataGridView dgv,string[] columnsName,int columnCount)
        {
            dgv.ColumnCount = columnCount;
            int i;
            for (i = 0; i < columnCount; i++)
            {
                dgv.Columns[i].Name = columnsName[i];
            }
        }

        private void DgvClasses_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            DataGridViewRow row = dgvClasses.Rows[rowIndex];
            string id = row.Cells["ID"].Value.ToString();
            MessageBox.Show("Data = " + id);
        }

      
    }
}
