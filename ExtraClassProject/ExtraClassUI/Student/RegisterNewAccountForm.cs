﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtraClassLibrary;
using ExtraClassLibrary.Models;

namespace ExtraClassUI.Student
{
    public partial class RegisterNewAccountForm : Form
    {
        private UserModel user;
        public RegisterNewAccountForm()
        {
            InitializeComponent();
        }

        private void Label6_Click(object sender, EventArgs e)
        {

        }

        private void BtnRegisterAccount_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                user = new UserModel(
                    txtUserName.Text.Trim(),
                    txtPassword.Text.Trim(),
                    txtEmail.Text.Trim(),
                    txtAddress.Text.Trim()
                    );
                GlobalConfig.Connection.RegisterUser(user);
                MessageBox.Show("Register new account succeed", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Failed to register new account.Please check it again", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool ValidateForm()
        {
            if(txtUserName.Text.Length == 0 || txtAddress.Text.Length == 0 || txtEmail.Text.Length==0 || txtPassword.Text.Length == 0)
            {
                return false;
            }
            return true;
        }

        private void LlbLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
        }
    }
}
