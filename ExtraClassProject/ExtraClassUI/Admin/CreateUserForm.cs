﻿using ExtraClassLibrary;
using ExtraClassLibrary.DataAccess;
using ExtraClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtraClassUI.Admin
{
    public partial class CreateUserForm : Form
    {
        private List<RoleModel> roles = GlobalConfig.Connection.GetAllRoles();
        private List<UserModel> users = GlobalConfig.Connection.GetAllUsers();
        public CreateUserForm()
        {
            InitializeComponent();
            SetUpUserListView();
            LoadRoles();
            UserListView();

            updateUserBtn.Enabled = false;
            deleteUserBtn.Enabled = false;

        }

        private bool ValidateForm()
        {
            bool output = true;
            if(emailTb.Text.Length == 0 || userNameTb.Text.Length == 0 || passwordTb.Text.Length == 0)
            {
                output = false;
            }

            if (!IsValidEmail(emailTb.Text))
            {
                output = false;
            }

            return output;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void LoadRoles()
        {
            roleCbb.DataSource = roles;
            roleCbb.DisplayMember = "RoleName";
            roleCbb.ValueMember = "Id";
        }

        private void UserListView()
        {
            userLv.Columns.Add("ID");
            userLv.Columns.Add("Email");
            userLv.Columns.Add("User Name");
            userLv.Columns.Add("Address");
            
            foreach (UserModel person in users)
            {
                ListViewItem item = new ListViewItem((person.Id).ToString());
                item.SubItems.Add(person.Email);
                item.SubItems.Add(person.UserName);
                item.SubItems.Add(person.Address);
                userLv.Items.AddRange(new ListViewItem[] {item});
            }
        }

        private void CreateUserBtn_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
              {
                  UserModel user = new UserModel(
                      userNameTb.Text,
                      passwordTb.Text,
                      emailTb.Text,
                      addressTb.Text,
                      Convert.ToInt32(roleCbb.SelectedValue));

                  GlobalConfig.Connection.CreateUser(user);
                  clear();
                  
                  MessageBox.Show("Added new user", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
              else
              {
                  MessageBox.Show("Invalid some text in Form. Please check it carefully", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
              }
                        
        }

        private void SetUpUserListView()
        {
            userLv.FullRowSelect = true;
            userLv.View = View.Details;
            userLv.AllowColumnReorder = true;
           // userLv.GridLines = true;
        }

        private void UserLv_SelectedIndexChanged(object sender, EventArgs e)
        {
           if (userLv.SelectedItems.Count > 0)
            {
                updateUserBtn.Enabled = true;
                deleteUserBtn.Enabled = true;
                ListViewItem selectedItem = userLv.SelectedItems[0];
                int id = Convert.ToInt32(selectedItem.Text);
                UserModel user = GlobalConfig.Connection.GetUserById(new UserModel(id));
                UpdateFillData(user);
            }

        }
       
        private void UpdateFillData(UserModel user)
        {
            userNameTb.Text = user.UserName;
            emailTb.Text = user.Email;
            addressTb.Text = user.Address;
            passwordTb.Text = user.Password;
            roleCbb.SelectedValue = user.IdRole;
        }

        private void clear()
        {
            userNameTb.Text = null;
            passwordTb.Text = null;
            emailTb.Text = null;
            addressTb.Text = null;
        }
        private void UpdateUserBtn_Click(object sender, EventArgs e)
        {
            ListViewItem selectedItem = userLv.SelectedItems[0];
            //  MessageBox.Show("Item : " + item.Text);
            int id = Convert.ToInt32(selectedItem.Text);
            if (ValidateForm())
            {
                UserModel user = new UserModel(
                    id,
                    userNameTb.Text,
                    passwordTb.Text,
                    emailTb.Text,
                    addressTb.Text,
                    Convert.ToInt32(roleCbb.SelectedValue)
                    );
                GlobalConfig.Connection.UpdateUser(user);
                MessageBox.Show("Update User succeeded", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                updateUserBtn.Enabled = false;
                
            }
            else
            {
                MessageBox.Show("Please check it carefully", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void DeleteUserBtn_Click(object sender, EventArgs e)
        {        
            DialogResult result = MessageBox.Show("Do you want to delete it?", "Delete Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(result == DialogResult.Yes)
            {
                ListViewItem selectedItem = userLv.SelectedItems[0];
                int id = Convert.ToInt32(selectedItem.Text);
                UserModel user = GlobalConfig.Connection.DeleteUser(new UserModel(id));
                MessageBox.Show("Deleted User", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
