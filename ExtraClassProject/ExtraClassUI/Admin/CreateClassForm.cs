﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtraClassLibrary;
using ExtraClassLibrary.Models;


namespace ExtraClassUI.Admin
{
    public partial class CreateClassForm : Form
    {
        private const string RoleTeacher = "teacher";
        private List<UserModel> teachers = GlobalConfig.Connection.GetUserByRole(new RoleModel(RoleTeacher));
        private List<ClassModel> classes = GlobalConfig.Connection.GetAllClass();
        public CreateClassForm()
        {
            InitializeComponent();
            FillTeacherCbb();
            SetUpListView();
            LoadListView();
            btnUpdateClass.Enabled = false;
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private bool ValidateForm()
        {
            bool output = true;
            if (classNameTb.Text.Length == 0 || descriptionTb.Text.Length == 0)
            {
                output = false;
            }
            return output;
        }

        private void FillTeacherCbb()
        {
            teacherCbb.DataSource = teachers;
            teacherCbb.DisplayMember = "TeacherInformation";
            teacherCbb.ValueMember = "Id";
        }

        private void LoadListView()
        {
            lstvClass.Columns.Add("ID");
            lstvClass.Columns.Add("Class");
            lstvClass.Columns.Add("Teacher");
            lstvClass.Columns.Add("Description");

            foreach (ClassModel model in classes)
            {
                ListViewItem item = new ListViewItem((model.Id).ToString());
                item.SubItems.Add(model.ClassName);
                UserModel teacher = GetUserById(model.IdTeacher);
                item.SubItems.Add(teacher.UserName);
                item.SubItems.Add(model.Description);
                lstvClass.Items.AddRange(new ListViewItem[] { item });
            }
        }

        private void SetUpListView()
        {
            lstvClass.FullRowSelect = true;
            lstvClass.View = View.Details;
            lstvClass.AllowColumnReorder = true;
            lstvClass.GridLines = true;
        }
        private void CreateClassBtn_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                ClassModel newClass = new ClassModel(
                    classNameTb.Text.Trim(),
                    Convert.ToInt32(teacherCbb.SelectedValue),
                    descriptionTb.Text.Trim()
                    );
                GlobalConfig.Connection.CreateClass(newClass);
                MessageBox.Show("Added class", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
            }
            else
            {
                MessageBox.Show("Failed to add new class", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void clear()
        {
            classNameTb.Text = null;
            descriptionTb.Text = null;
        }
        private UserModel GetUserById(int id)
        {
            UserModel user = GlobalConfig.Connection.GetUserById(new UserModel(id));
            return user;
        }

        private void LstvClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lstvClass.SelectedItems.Count > 0)
            {
                btnUpdateClass.Enabled = true;
                ListViewItem selectedItem = lstvClass.SelectedItems[0];
                int id = Convert.ToInt32(selectedItem.Text);
                ClassModel model = GlobalConfig.Connection.GetClassById(new ClassModel(id));
                FillDataForm(model);
            }
        }

        private void FillDataForm(ClassModel model)
        {
            classNameTb.Text = model.ClassName;
            teacherCbb.SelectedValue = model.IdTeacher;
            descriptionTb.Text = model.Description;

        }

        private void BtnUpdateClass_Click(object sender, EventArgs e)
        {
            ListViewItem selectedItem = lstvClass.SelectedItems[0];
            int id = Convert.ToInt32(selectedItem.Text);

            if (ValidateForm())
            {
                ClassModel model = new ClassModel(
                    id,
                    classNameTb.Text,
                    Convert.ToInt32(teacherCbb.SelectedValue),
                    descriptionTb.Text
                    );
                GlobalConfig.Connection.UpdateClass(model);
                MessageBox.Show("Update Class succeeded", "Success Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                btnUpdateClass.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please check it carefully", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnViewMore_Click(object sender, EventArgs e)
        {

        }
    }
}
