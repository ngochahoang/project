create table person(
	id int IDENTITY(1,1) primary key,
	username varchar(255) not null,
	email varchar(255) not null unique,
	password varchar(255) not null,
	address text,
	idrole int foreign key references role(id) not null

);
select * from person;

create table role(
	id int identity(1,1) primary key,
	rolename varchar(100) not null
);


create table class(
	id int identity(1,1) primary key,
	classname varchar(255) not null,
	idteacher int foreign key references person(id) not null,
	description text
)

create table class_person(
	id int identity(1,1) primary key,
	idclass int foreign key references class(id) not null,
	idstudent int foreign key references person(id) not null
	)

create table score(
	id int identity(1,1) primary key,
	class_id int foreign key references class(id) not null,
	student_id int foreign key references person(id) not null,
	score float not null
)

select * from role;
insert into role (role) values ('Admin');
insert into role (role) values ('Teacher');
insert into role (role) values ('Student');

create procedure sp_get_all_roles
as
begin
	select * from role 
end

exec sp_get_all_roles

--Insert new user
create procedure sp_user_insert
	@username nvarchar(255),
	@email nvarchar(255),
	@password nvarchar(255),
	@address text,
	@idrole int,
	@id int = NULL OUTPUT
	
as
begin
	set nocount on
	insert into person (username,email,password,address,idrole) values (@username,@email,@password,@address,@idrole);
	SET @id = SCOPE_IDENTITY();
end

select * from person;
exec sp_user_insert @username='Hoai Thuong',@email='hoaithuong@gmail.com',@password='123456',@address='Quang Binh',@idrole=2

--GetAllUsers
create procedure sp_get_all_person
as
begin
	select * from person
end

exec sp_get_all_person

--GetUser
create procedure sp_get_person
	@id int
as 
begin
   select * from person where id=@id
end
exec sp_get_person @id=1

--UpdateUser

create procedure sp_update_person
	@id int output,
	@username nvarchar(255),
	@email nvarchar(255),
	@password varchar(255),
	@address text,
	@idrole int
as
begin
	update person set username=@username,email=@email,password=@password,address=@address,idrole=@idrole 
	where id=@id
end

exec sp_update_person @id=1,@username='ngocha123456',@email='ngocha080997@gmail.com',@password='123456',@address='Da Nang city',@idrole=1

--DeleteUser

create procedure sp_delete_person
@id int
as
begin
	delete from person where id=@id
end

exec sp_delete_person @id=9

--GetUserByRole (Role = Teacher)

create procedure sp_get_user_by_role
  @rolename varchar(100)
as
begin
	select person.id,person.username,person.email from person inner join
	role on person.idrole=role.id where role.rolename=@rolename
end

exec sp_get_user_by_role @rolename='teacher';


-- Create new class
create procedure sp_class_insert
	@classname varchar(255),
	@idteacher int,
	@description text,
	@id int = NULL OUTPUT
as
begin
	set nocount on
	insert into class(classname,idteacher,description) values(@classname,@idteacher,@description)
	set @id=SCOPE_IDENTITY()
end

exec sp_class_insert @classname='class2',@idteacher=4,@description='This is description for class2'

--Get All Class;
create procedure sp_get_all_class
as
begin
	select class.classname,class.description,person.username,class.idteacher,class.id from class inner join
	person on class.idteacher=person.id
end
select * from person
exec sp_get_all_class
select * from class;
create procedure sp_get_class
	@id int
as 
begin
	select * from class where id=@id
end

--Update Class
create procedure sp_update_class
@id int,
@classname varchar(255),
@description text,
@idteacher int
as
begin
	update class set classname=@classname,description=@description,idteacher=@idteacher where id=@id
end

--Register new account

create procedure sp_user_register
	@username nvarchar(255),
	@email nvarchar(255),
	@password nvarchar(255),
	@address text,
	@id int = NULL OUTPUT
	
as
begin
	set nocount on
	declare @idrole as int
	set @idrole = (select id from role where rolename='Student')
	insert into person (username,email,password,address,idrole) values (@username,@email,@password,@address,@idrole);
	SET @id = SCOPE_IDENTITY();
end

exec sp_user_register @username='student2',@email='student2@gmail.com',@password='123456',@address='Hue'
exec sp_get_all_person
--

--Login
create procedure sp_login
@email varchar(255),
@password varchar(255)
as
begin
	declare @id as int
	select * from person where email=@email and password=@password
	set @id = (select id from person where email=@email and password=@password)
end

exec sp_login @email='ngocha@gmail.com',@password='123'
--
--get role by id
create procedure sp_get_role
	@id int
as 
begin
   select * from role where id=@id
end
--

--register new class
create procedure sp_register_class
@idclass int,
@idstudent int,
@id int output
as
begin	
	set nocount on
    insert into class_person(idclass,idstudent) values (@idclass,@idstudent);
	set @id=SCOPE_IDENTITY()
end
--

select * from class;

select * from person;
select * from role;


exec sp_get_all_class
select * from person;
select * from class;
select *from class_person;